Ext.define('test.view.login.LoginModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.login',

    data: {
        login: '',
        password: '',
        rememberMe: false
    },
    formulas: {
        disableClearButton: function (get) {
            return !(get('login').length || get('password').length || get('rememberMe'));
        },

        disableLoginButton: function (get) {
            return !(!!get('login').length && !!get('password').length);
        }
    }
});

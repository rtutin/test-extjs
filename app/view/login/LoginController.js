Ext.define('test.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',
    viewModel: 'login',

    onLoginClick: function() {
        localStorage.setItem("LoggedIn", true);

        this.getView().destroy();

        Ext.create({
            xtype: 'app-main'
        });
    },

    onClearClick: function() {
        var viewModel = this.getViewModel();
        viewModel.set('login', '');
        viewModel.set('password', '');
        viewModel.set('rememberMe', false);
    },
});
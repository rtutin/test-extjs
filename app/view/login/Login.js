Ext.define('test.view.login.Login', {
    extend: 'Ext.window.Window',
    xtype: 'login',

    requires: [
        'test.view.login.LoginController',
        'Ext.form.Panel'
    ],

    controller: 'login',
    viewModel: 'login',
    bodyPadding: 10,
    title: 'Login Window',
    closable: false,
    autoShow: true,

    items: {
        xtype: 'form',
        reference: 'form',
        items: [
            {
                xtype: 'textfield',
                name: 'username',
                fieldLabel: 'User name or email',
                bind: '{login}'
            },
            {
                xtype: 'textfield',
                name: 'password',
                inputType: 'password',
                fieldLabel: 'Password',
                bind: '{password}'
            },
            {
                xtype: 'checkbox',
                boxLabel: 'Remember',
                bind: '{rememberMe}'
            }
        ],
        buttons: [
            {
                text: 'Clear',
                listeners: {
                    click: 'onClearClick'
                },
                bind: {
                    disabled: '{disableClearButton}'
                }
            },
            {
                text: 'Login',
                listeners: {
                    click: 'onLoginClick'
                },
                bind: {
                    disabled: '{disableLoginButton}'
                }
            }
        ]
    }
});

Ext.define('test.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onLogoutClick: function () {
        localStorage.removeItem('LoggedIn');
        this.getView().destroy();
        Ext.create({
            xtype: 'login'
        });
    }
});

Ext.define('test.view.employeesList.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'employeesList',
    reference: 'el',

    requires: [
        'test.store.Employees',
        'test.store.EmployeesChainedStore',
        'test.view.employeesList.ListController',
    ],

    controller: 'list',

    tools: [
        {
            type: 'close',
            handler: 'remove'
        },
        {
            type: 'plus',
            handler: 'add'
        },
        {
            type: 'refresh',
            handler: 'refresh'
        },
    ],

    // Tools in toolbar can be a buttons with text
    // tools: [
    //     {
    //         type: 'removeTool',
    //         renderTpl: ['<button>remove</button>'],
    //         handler: 'remove'
    //     },
    //     {
    //         type: 'addTool',
    //         renderTpl: ['<button>add</button>'],
    //         handler: 'add'
    //     },
    //     {
    //         type: 'refreshTool',
    //         renderTpl: ['<button>refresh</button>'],
    //         handler: 'refresh'
    //     },
    // ],

    selModel: {
        mode: 'MULTI'
    },

    store: 'EmployeesChainedStore',

    plugins:[{
        ptype: 'cellediting',
        clicksToEdit: 1
    }],

    columns: [
        { text: 'Name',  dataIndex: 'name', editable: true, editor: 'textfield' },
        { text: 'Age', dataIndex: 'age', flex: 1, editable: true, editor: 'textfield' },
        { text: 'Nickname', dataIndex: 'nickname', flex: 1, editable: true, editor: 'textfield' },
        { text: 'Employee', dataIndex: 'isEmployee', flex: 1, editable: true, editor: 'textfield' }
    ]
});
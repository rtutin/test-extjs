Ext.define('test.view.employeesList.ListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.list',

    remove: function() {
        this.getView().getSelectionModel().getSelection().forEach(function (item) {
            Ext.getStore('EmployeesChainedStore').remove(item);
        });
    },

    add: function() {
        var store = Ext.getStore('EmployeesChainedStore');
        var selected = this.getView().getSelectionModel().getSelection()[0];

        if (selected) {
            store.insert(store.indexOf(selected) + 1, {});
        } else {
            store.insert(store.data.length, {});
        }
    },

    refresh: function() {
        Ext.getStore('EmployeesChainedStore').getSource().reload();
    },
});

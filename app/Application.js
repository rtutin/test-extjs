Ext.define('test.Application', {
    extend: 'Ext.app.Application',
    name: 'test',
    stores: ['Employees', 'EmployeesChainedStore'],

    requires: [
        'Ext.grid.Panel'
    ],

    launch: function () {
        var loggedIn;
        loggedIn = localStorage.getItem("LoggedIn");
        Ext.create({
            xtype: loggedIn ? 'app-main' : 'login'
        });
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
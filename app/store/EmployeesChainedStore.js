Ext.define('test.store.EmployeesChainedStore', {
    extend: 'Ext.data.ChainedStore',
    source: Ext.create('Ext.data.Store', {
        model: 'test.model.Employee',
        data : [
            { name: 'Jean Luc', age: '30', nickname: 'jl', isEmployee: true },
            { name: 'Worf', age: '30', nickname: 'worf', isEmployee: true },
            { name: 'Deanna', age: '30', nickname: 'deanna', isEmployee: true },
            { name: 'Data', age: '30', nickname: 'data', isEmployee: true },
        ]
    })
});
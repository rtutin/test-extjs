Ext.define('test.store.Employees', {
    extend: 'Ext.data.Store',
    alias: 'store.employees',
    model: 'test.model.Employee'
});
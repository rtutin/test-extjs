Ext.define('test.model.Employee', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    schema: {
        id: 'employeeSchema',
        namespace: 'test.model',
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                rootProperty: 'items'
            }
        }
    },

    fields: [
        { name: 'name', type: 'string' },
        { name: 'age', type: 'string' },
        { name: 'nickname', type: 'string' },
        { name: 'isEmployee', type: 'boolean' }
    ]
});